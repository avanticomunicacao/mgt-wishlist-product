<?php

namespace Avanti\WishlistProduct\Helper;

use Magento\Wishlist\Model\ResourceModel\Item\CollectionFactory as WishlistCollectionFactory;
use Magento\Customer\Model\SessionFactory as CustomerSessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;

class ProductWishlist extends AbstractHelper
{
    protected $wishlistCollection;
    protected $customerSession;

    public function __construct(WishlistCollectionFactory $wishlistCollection, CustomerSessionFactory $customerSession)
    {
        $this->wishlistCollection = $wishlistCollection;
        $this->customerSession = $customerSession;
    }

    public function productIsOnWishlist($productId)
    {
        $customer = $this->customerSession->create();
        if ($customer->isLoggedIn()) {
            $wishlist = $this->wishlistCollection->create()->addCustomerIdFilter($customer->getCustomerId());
            foreach ($wishlist as $item) {
                if ($item->getProductId() === $productId) {
                    return true;
                }
            }
        }
        return false;
    }
}
