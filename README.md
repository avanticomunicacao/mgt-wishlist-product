
#Module Wishlst Product
O módulo verificar se um produto está adicionado a wishlist do usuário.

#Uso
Para usar, referencie o Helper **"ProductWishlist** e chame o método **productIsOnWishlist**.
Esse método irá retornar ou true caso o produto estiver na wishlist, ou nada se ela não estiver.
para usar fazer um if ($helper->productIsOnWishlist($idProduct)). Se o produto estiver na whishlit, vair entrar no if.
